extends CharacterBody2D

var speed = 450
var positionDelta = 10
var ball

func _ready():
	ball = get_parent().get_node("Ball")
	
func _physics_process(delta):
	
	# @TODO mandar mensajes en vez de tener la referencia de la pelota
	# La pelota mandaría un mensaje con su posición
	# Computer estaría escuchando el mensaje (¿Patrón observer?)
	# Añadir niveles de dificultad para la IA, esto es, lo capaz o incapaz que es de seguir
	# la trayectoria de la pelota
	
	velocity.y = 0
	if abs(ball.position.y - position.y) > positionDelta:
		followBall(delta)

func followBall(delta) -> void:
	
	if ball.position.y < position.y:
		velocity.y = -1
	elif ball.position.y > position.y:
		velocity.y = 1
	else:
		velocity.y = 0
		
	velocity.y *= speed
	move_and_collide(velocity * delta)
