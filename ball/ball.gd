extends CharacterBody2D

var speed = 500

func _ready():
	resetSpeed()
	
func _physics_process(delta):
	var collisionInfo = move_and_collide(velocity * delta)
	if (collisionInfo):
		velocity = velocity.bounce(collisionInfo.get_normal())

func resetSpeed():
	velocity.x = 1 if (randi() % 2) else -1
	velocity.y = 1 if (randi() % 2) else -1
	velocity *= speed
