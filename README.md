# Compiler's Godot Pong

Versión simple de Pong para ir aprendiendo Godot 4

## Referencia

* https://www.youtube.com/watch?v=YCTPNRw1EXo
* https://github.com/indierama/PongTutorial/

## TO-DO

* Niveles de dificultad - IA del oponente
* Límite de puntuación - Final de partida
* Sonidos
* Dirección inicial de la pelota en función de quién metió gol
* Desacoplamiento de entidades.
* Menú de opciones:
	* Attract mode
	* 1/2 jugadores
