extends Node2D

const CENTER = Vector2(640, 360)

var playerScore = 0
var computerScore = 0

func _on_left_goal_body_entered(_body):
	computerScore += 1
	updateScore()
	reset()

func _on_right_goal_body_entered(_body):
	playerScore += 1
	updateScore()
	reset()

func reset():
	$Ball.position = CENTER
	$Ball.call("resetSpeed")
	$Player.position.y = CENTER.y
	$Computer.position.y = CENTER.y

func updateScore():
	$Score.call("setScore", playerScore, computerScore)
