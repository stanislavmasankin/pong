extends StaticBody2D

func _ready():
	
	position = Vector2(640, 60)
	$PlayerScore.position = Vector2(-160, 0)
	$ComputerScore.position = Vector2(40, 0)
	setScore(0, 0)

func setScore(playerScore: int, computerScore: int) -> void:
	
	$PlayerScore.text = str(playerScore)
	$ComputerScore.text = str(computerScore)
