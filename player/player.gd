extends CharacterBody2D

var speed = 500;

func _physics_process(delta):

	velocity.y = Input.get_axis("ui_up", "ui_down")
	velocity.y *= speed

	move_and_collide(velocity * delta)
